
Nicey 2.0
=========

New implementation of python library for NES (Nicey Enterprise System).

A couple of notes:
------------------
Some functions will not return anything unless the Nicey contructor is passed a order_id.
The order_id is the "nicey_id", if set calls to new_payment will upsell.
Without a order_id a new order_id will be assigned to the Nicey object after success.


API:
----
#### New Order (new user):

    >>> from nicey2 import Nicey2
    >>> n = Nicey2(debug=False)
    >>> n.order_id
    None
    >>> n.get_credit_card()
    None
    >>> n.add_card(number='411111111111', expiration='10/14', cvv='12', nickname="mycard")
    True
    >>> n.modify_card('mycard', default=True)
    True
    >>> n.new_purchase(price="3.26", ip="123.23.21.21", description="bought some things")
    (564, 235)
    >>> n.order_id
    3654

#### Adding a purchase to an existing order:

    >>> from nicey2 import Nicey2
    >>> n = Nicey2(order_id=3654, debug=False)
    >>> n.modify_card('mycard')
    True
    >>> n.new_purchase(price="93.01", ip="123.23.21.21", description="bought some more things")
    (598, 289)


Testing:
--------
To run all the tests, execute the following command:

    nosetests --with-coverage --cover-package=nicey2

To run the some of tests, execute one of the following commands:

    nosetests --with-coverage --cover-package=nicey2 -w=tests/functional
    nosetests --with-coverage --cover-package=nicey2 -w=tests/unit
