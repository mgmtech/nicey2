import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

requires = [
    'nose',
    'minimock',
    'ordereddict',
]

setup(
    name = "nicey2",
    version = "0.3.6",
    author = "Matt Miller",
    author_email = "matthewgarrettmiller@gmail.com",
    description = ("A nicey billing API."),
    license = "Internal",
    keywords = "nicey billing api",
    packages=['nicey2'],
    package_dir={'nicey2':'src/nicey2'},
    long_description=README,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Libraries",
    ],
    install_requires = requires,
    tests_require = requires,
    zip_safe = False,
)
