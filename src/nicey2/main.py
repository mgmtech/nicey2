"""
Nicey 2.0 library implemention

Nicey2 XSD require certain elements appear in a specific order when making the
requests. PLEASE refer to the service and order XSD from nicey before making
any changes to the order in which things are added to OrderedDict or Tuples.
"""
import logging

import httplib

from pprint import pprint

import re

from decimal import Decimal

from nicey_exceptions import *
from nicey_responses import *

from utils import dict2tuple
from utils import find_card_details

from xmlgateway import XMLGatewayInterface

logger = logging.getLogger('nicey2')

try:
    from django.conf import settings
    NICEY_DEBUG = settings.NICEY_DEBUG
except ImportError:
    NICEY_DEBUG = False

class NiceyMachinery(object):
    """ Represents the base Nicey cgi client executing the various api requests. """
    CGI_MAP = {
        'Order' : '/api/order.cgi',
        'Service' : '/api/service.cgi',
        'ExportRequest' : '/api/export.cgi',
        'Upsell' : '/api/upsell.cgi'
    }
    DEFAULT_ERROR_MESSAGE = 'There was an issue with processing your request.'


    def __init__(self, customer_id=None, order_id=None, debug=None, ssl=True):
        """
        params:
            debug (True or False) - Print put verbose info to the log/console about
                                    each and every remote call.
        """
        self.order_id = order_id
        self.customer_id = customer_id
        self.debug = NICEY_DEBUG if debug is None else debug

        if not self.API_HOST:
            raise NiceyError('You must subclass this object with user/pass and host')


    def execute(self, client):
        """
        Execute the call to nicey and return the response as an object. But first,
        set the username/password to the XML client, add the order ID, and also
        get the URI by introspecting the client XML.
        the method argument must be either 'upsell', 'lookup' or 'set'
        """
        uri = self._get_uri(client)

        try:
            response = client.submit(uri)
        except httplib.socket.gaierror:
            raise NiceyServerError('The Nicey server is down or unreachable')

        error = response.get_error()
        if error:
            raise NiceyResponseError(error)
        else:
            self.check_response_errors(response)
        return response

    def check_response_errors(self, response):
        """
        Checks the response document for any errors. This should prevent any
        future propogation and ultimately failures in the ui for the user.
        """
        if response.dom:
            error_tag = response.dom.getElementsByTagName('Error')
            if error_tag:
                message = error_tag[0].firstChild.nodeValue
                raise NiceyResponseError(message)
            error_tag = response.dom.getElementsByTagName('Exception')
            if error_tag:
                message = error_tag[0].firstChild.nodeValue
                raise NiceyResponseError(message)
            transaction_tag = response.dom.getElementsByTagName('Transaction')
            if transaction_tag:
                approved_attr = transaction_tag[0].getAttribute('Approved')
                if approved_attr and approved_attr != 'true':
                    raise NiceyResponseError(transaction_tag[0].getAttribute('Response'))


    def _get_uri(self, client):
        """
        Try to get the request URI for the next remote call based on the path of
        the XML gateway client. TODO: make it obvious that the xml_path_root
        is being set as a member variable and then used again on the .execute()
        method.
        """
        xml_path_root = client.doc.childNodes[0].tagName
        # map supported API module calls to get the API uri
        if not xml_path_root in self.CGI_MAP:
            raise NiceyInternalError("Dont know how to handle %s, its not in my CGI_MAP!" % xml_path_root)
        else:
            return self.CGI_MAP[xml_path_root]


class Nicey2(NiceyMachinery):
    """
    Represents the Nicey2 Client Interface used to relay core 
    billing related api calls from listing cards to making transactions.
    """

    # subclasses should write these in
    API_PASS = ''
    API_USER = ''
    API_HOST = ''
    WEBSITE = ''
    MERCHANT = ''
    CURRENCY = ''

    def __init__(self, *args, **kwargs):
        self.addresses_tuples = {}
        self.cc_num = None
        super(Nicey2, self).__init__(*args, **kwargs)


    def authenticate(self, client, parent_element):
        """
        Using the given XMLGatewayInterface client, this should authenticate 
        with the Nicey interface with the configured API_USER and API_PASS.
        """
        client.set('%s/Authentication/Username' % parent_element, self.API_USER)
        client.set('%s/Authentication/Password' % parent_element, self.API_PASS)


    def client_connect(self, parent_element):
        """
        Creates a new XMLGatewayInterface client and authenticates on the given 
        parent element within the Nicey service provider.
        """
        client = XMLGatewayInterface(self.API_HOST, ssl=True, debug=self.debug)
        self.authenticate(client, parent_element)
        return client

    def new_purchase(self, price, description, ip=None):
        """
        Add a new purchase to the Nicey Order that this class represents. Before
        any purchase can be made, an exiting order_id must be already created,
        -OR- all the info must be avaliable to make an new order_id.
        """
        order_tuple = (
            ('Website', self.WEBSITE),
            ('MerchantPool',self.MERCHANT),
            ('Description', description),
            ('Currency', self.CURRENCY),
            ('Upsell', 'true' if self.customer_id else 'false'),
            ('InitialPreauth', 'false'),
            ('InitialPrice', price),
        )

        return self._create_order(order_tuple, ip, customer_id=self.customer_id)


    def _create_order(self, order_tuple, ip, customer_id=None):
        """
        Assuming we have everything we need, crate the order and return the Order
        ID. Refer to the nicey docs for details about this stuff.
        """
        client = self.client_connect('Order')
        client.set('Order/Customer', None, 'ID:%s' % (customer_id) if customer_id else None)

        # agent info element
        if ip:
            client.set('Order/AgentInfo', None, None)
            client.set('Order/AgentInfo/IPAddress', ip)

        # products element
        client.set('Order/Products', attribute='Mode:AllOrNothing|EchoSalePolicy:true')
        client.set('Order/Products/Dynamic', order_tuple)

        # customer info elements
        if not customer_id:
            client.set('Order/Customer/ContactInfo', self.addresses_tuples['Contact'], 'Usage:Contact')
            client.set('Order/Customer/ContactInfo', self.addresses_tuples['Billing'], 'Usage:Billing')

            client.set('Order/Customer/PaymentAccount', False, 'Type:CreditCard')
            client.set('Order/Customer/PaymentAccount/ccnum', self.cc_num)
            client.set('Order/Customer/PaymentAccount/ccexp', self.cc_exp)
            client.set('Order/Customer/PaymentAccount/cvv', self.cc_cvv)

        resp_dom = self.execute(client).dom

        # Set the customer id if we do not currently have one at this moment
        if not customer_id:
            self.customer_id = int(resp_dom.getElementsByTagName('Customer')[0].getAttribute('ID'))

        order = resp_dom.getElementsByTagName('Order')[0]
        purchase = order.getElementsByTagName('Purchase')[0]
        transaction = purchase.getElementsByTagName('Transaction')[0]
        merchant = transaction.getElementsByTagName('Merchant')[0]

        res = Nicey2Response()
        res.order_id=int(order.getAttribute('ID'))
        res.purchase_id=int(purchase.getAttribute('ID'))
        res.transaction_id=int(transaction.getAttribute('ID'))
        res.approved=transaction.getAttribute('Approved').lower() == 'true'
        res.response=transaction.getAttribute('Response').lower()
        res.customer_id=self.customer_id
        res.statement_descriptor=merchant.getAttribute('StatementDescriptor')
        return res


    def lookup_transaction(self, transaction_id):
        client = self.client_connect('Service')
        client.set('Service/Transaction', False, 'ID:%s' % transaction_id)
        resp_dom = self.execute(client).dom

        transaction = resp_dom.getElementsByTagName('Transaction')[0]
        approved = transaction.getElementsByTagName('Approved')[0].firstChild.nodeValue

        return dict(
            id=int(transaction.getAttribute('ID')),
            amount=Decimal(transaction.getElementsByTagName('SettlePrice')[0].firstChild.nodeValue),
            purchase_id=int(transaction.getAttribute('PurchaseID')),
            approved=bool(approved == 'true'),
        )


    def refund_transaction(self, transaction_id, amount=None, description=''):
        """
        Refunds a transaction. The amount was added to implement partial
        refund functionality. Type field was added for future
        implementation. Currnetly only transactions are supported.
        """
        transaction = self.lookup_transaction(transaction_id)
        if amount:
            if amount > transaction.get('amount'):
                raise ValueError('The amount to refund must be less than or equal to the total transaction amount.')
        else:
            amount = transaction.get('amount')

        client = self.client_connect('Service')
        client.set('Service/Transaction', False, 'ID:%s' % transaction_id)

        if amount:
            client.set('Service/Transaction/Reverse', False, 'Amount:%s|Description:%s' % (amount, description))
        else:
            client.set('Service/Transaction/Reverse', False, 'Description:%s' % (description))

        resp_dom = self.execute(client).dom
        transaction_reverse = resp_dom.getElementsByTagName('Reverse')[0]
        approved = transaction_reverse.getAttribute('Approved')
        transaction_id = transaction_reverse.getAttribute('ID')

        return dict(
            id=int(transaction_id),
            approved=bool(approved == 'true')
        )


    def add_contact(self, contact):
        if hasattr(contact, 'to_nicey_dict'):
            contact = dict2tuple(contact.to_nicey_dict())
        else:
            contact = dict2tuple(contact)
        client = self.client_connect('Service')
        client.set('Service/Customer', False, 'ID:%s' % self.customer_id)
        client.set('Service/Customer/NewCustomerContact', contact, 'Description:Contact')

        response = self.execute(client)
        try:
            return int(response.NewCustomerContact.CustomerContact.attribute.ID)
        except ValueError:
            return False
        return False


    def add_address(self, address, info_type=None):
        """
        'address' must be a dictionary with the keys starting in capital letters
        Also, country must be a two letter code, e.g: 'US'

        -OR- 'address' can be an address object, but it must have a 'to_niey_dict'
        method that returns a dict object as described below.

        The different values for 'info_type' can be either "Billing", "Contact",
        and 'Shipping'. These values muct be titlecase as well.

        address = (
                {'Name': customer.get_full_name(),
                 'Email': customer.email,
                 'Phone': profile.phone_number,
                 'Address': address,
                 'City': shipping_address.city,
                 'State': shipping_address.state,
                 'Zip': shipping_address.zipcode,
                 'Country': shipping_address.country}
        """

        if not info_type:
            raise ValueError("Second argument must be either 'Contact', 'Billing' or 'Shipping'")


        address_tuple = tuple(address.items())

        if not self.order_id:
            # save for later when we execute the purchase
            self.addresses_tuples[info_type] = address_tuple
            if self.debug:
                print "**DEBUG**: address not sent to nicey yet; make a purchase first"
            return True

        client = self.client_connect('Service')
        client.set('Service/Order', False, 'ID:%s' % self.order_id)

        # setting the contact info
        type_ = 'Type:%s' % info_type
        client.set('Service/Order/Update/Info', address_tuple, type_)

        response = self.execute(client)

        return True


    def list_cards(self):
        """
        Get the users current payment methods, returning a list of
        dictionaries.
        e.g:
            {'primary': True,
             'active': True,
             'id': 8984,
             'nickname': 'asdas',
             'type': either 'Visa', 'Mastercard', 'Amex' or 'Discover'
             'last4': 1232,
             }
        """

        client = self.client_connect('Service')
        # pretty much hide everything but the contact information and payment accounts
        client.set('Service/Customer', None, 'ID:%s|ViewOrders:false|ViewTransactions:false|ViewPurchases:false' % self.customer_id)
        response = self.execute(client)
        resp_dom = response.dom

        payment_accounts = resp_dom.getElementsByTagName('PaymentAccounts')[0]
        payment_accounts = payment_accounts.getElementsByTagName('PaymentAccount')

        contacts = resp_dom.getElementsByTagName('Contact')

        cc_list = []
        for payment in payment_accounts:
            # Does the same work we are doing down in get_latest_order
            # REFACTOR
            nickname = payment.getElementsByTagName('NickName')[0].firstChild.nodeValue
            default = payment.getAttribute('Default')
            active = payment.getAttribute('Active')
            safe_identifier = payment.getElementsByTagName('SafeIdentifier')[0]
            safe_identifier = safe_identifier.firstChild.nodeValue
            sub_payment_type = (payment.
                                getElementsByTagName('SubPaymentType')[0].
                                firstChild.
                                nodeValue)
            card_details = find_card_details(safe_identifier)

            info_dict = {
                'id': payment.getAttribute('ID'),
                'nickname': nickname,
                'default': bool(default == 'true'),
                'active': bool(active == 'true'),
                'type': sub_payment_type,
                'last4': card_details['last4'],
                'exp': card_details['exp'],
            }
            contact_id = payment.getAttribute('BillingContactID')

            for contact in contacts:
                if contact.getAttribute('ID') == contact_id:
                    info_dict['zipcode'] = contact.getElementsByTagName('Zip')[0].firstChild.nodeValue
                    break

            cc_list.append(info_dict)
        return cc_list


    def get_default_card(self):
        """
        Get the card that is marked as 'default' in nicey.
        """
        cards = self.list_cards()
        if cards:
            for card in cards:
                if card.get('default') and card.get('active'):
                    return card


    def get_default_last4(self, formatted=None):
        """
        get the last4 digits of a credit card from nicey
        """
        default_cc = self.get_default_card()
        if not default_cc:
            return None
        return default_cc.get('last4')


    def add_card(self, card, nickname, contact_id, default=True):
        """
        Add a card to this order. Card must be a dict:
        {'ccnum': 345345, 'cvv': 456, 'exp': '10/2012')
        """
        cc_tuple = tuple(card.items())

        if not self.customer_id:
            # save for when we create the order (when submitting the purchase)
            if self.debug:
                print "**DEBUG**: card not sent to nicey yet; make a purchase first"
            self.cc_num = card['ccnum']
            self.cc_cvv = card['cvv']
            self.cc_exp = card['ccexp']
            return True
        
        client = self.client_connect('Service')
        client.set('Service/Customer', False, 'ID:%s' % self.customer_id)

        nick = 'NickName:%s|BillingContactID:%s|PaymentType:Credit Card' % (nickname, contact_id)

        client.set('Service/Customer/NewPaymentAccount', cc_tuple, nick)
        # POST to nicey
        response = self.execute(client)

        payment_account_id = None

        if hasattr(response.NewPaymentAccount, 'DuplicateAccount'):
            payment_account_id = int(response.NewPaymentAccount.DuplicateAccount.attribute.ID)
            self.modify_card(payment_account_id, active=True, default=default)

        elif hasattr(response.NewPaymentAccount, 'PaymentAccount'):
            payment_account_id = int(response.NewPaymentAccount.PaymentAccount.attribute.ID)
        else:
            raise NiceyRequestError('No PaymentAccount ID was returned from the billing gateway.')

        return payment_account_id


    def modify_card(self, account_id, active=False, default=False):
        """
        Take the PaymentAccountID and update the Active and Default flags.
        This method has a weird signature and should be redone.

        If Default is true, active is implied, so it will send two requests.
        Else it will update the Active flag only.
        """
        if default:
            client = self.client_connect('Service')
            client.set('Service/Customer', None, 'ID:%s' % self.customer_id)
            client.set('Service/Customer/DefaultPaymentAccount', None, 'ID:%s' % account_id)
            response = self.execute(client)
            active = True

        client = self.client_connect('Service')
        client.set('Service/PaymentAccount', None, 'ID:%s' % account_id)
        client.set('Service/PaymentAccount/Active', 'true' if active else 'false')
        response = self.execute(client)
        return int(response.PaymentAccount.attribute.ID)


    def get_latest_order(self):
        resp_dom = self.lookup_customer().dom

        payment = resp_dom.getElementsByTagName('PaymentAccount')[0]

        payment_account_id = payment.getAttribute('ID')

        safe_identifier = payment.getElementsByTagName('SafeIdentifier')[0]
        safe_identifier = safe_identifier.firstChild.nodeValue

        sub_payment_type = (payment.
                            getElementsByTagName('SubPaymentType')[0].
                            firstChild.
                            nodeValue)

        card_details = find_card_details(safe_identifier)

        initial_price = (resp_dom.
                      getElementsByTagName('Order')[0].
                      getElementsByTagName('SaleTerms')[0].
                      getElementsByTagName('InitialPrice')[0]
                      .firstChild.nodeValue)

        contact = resp_dom.getElementsByTagName('Contact')[0]

        return dict(
            card_type = sub_payment_type,
            card_num = card_details['last4'],
            payment_account_id = payment_account_id,
            city = contact.getElementsByTagName('City')[0].firstChild.nodeValue,
            region = contact.getElementsByTagName('State')[0].firstChild.nodeValue,
            zipcode = contact.getElementsByTagName('Zip')[0].firstChild.nodeValue,
            country = 'US',
            amount = initial_price,
        )


    def lookup_customer(self):
        client = self.client_connect('Service')
        client.set('Service/Customer', None, 'ID:%s' % self.customer_id)
        response = self.execute(client)
        return response

    def lookup_customers_by_last4(self, last4):
        '''
        Returns list of Customer IDs 
        '''
        client = self.client_connect('Service')

        client.set('Service/Lookup/PaymentAccount', last4)
        response = self.execute(client)

        return [x.getAttribute('ID') for x in response.dom.getElementsByTagName('Customer')]

    def lookup_order(self, order_id):
        client = self.client_connect('Service')
        client.set('Service/Order', None, 'ID:%s' % order_id)
        response = self.execute(client)
        return response


    def lookup_payment_account(self, account_id):
        client = self.client_connect('Service')
        client.set('Service/PaymentAccount', None, 'ID:%s' % account_id)
        resp_dom = self.execute(client).dom

        payment = resp_dom.getElementsByTagName('PaymentAccount')[0]
        nickname = payment.getElementsByTagName('NickName')[0].firstChild.nodeValue
        default = payment.getAttribute('Default')
        active = payment.getAttribute('Active')
        safe_identifier = payment.getElementsByTagName('SafeIdentifier')[0]
        safe_identifier = safe_identifier.firstChild.nodeValue
        sub_payment_type = (payment.
                            getElementsByTagName('SubPaymentType')[0].
                            firstChild.
                            nodeValue)
        card_details = find_card_details(safe_identifier)

        card = {
            'id': account_id,
            'nickname': nickname,
            'default': bool(default == 'true'),
            'active': bool(active == 'true'),
            'type': sub_payment_type,
            'last4': card_details['last4'],
            'exp': card_details['exp'],
        }
        return card
