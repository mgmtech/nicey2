import logging
import traceback
import sys

logger = logging.getLogger('excpetions')

__all__ = ['NiceyError', 'NiceyRequestError', 'NiceyResponseError',
           'GatewayError', 'RequestError', 'NiceyServerError']

class NiceyError(Exception):
    def __init__(self, *args, **kwargs):
        super(NiceyError, self).__init__(*args, **kwargs)
        callstack = []
        for depth, stack in enumerate(traceback.format_stack()):
            callstack.append(stack)
            try:
                callstack.append(str(sys._getframe(depth).f_locals))
            except:
                # python docs say _getframe may not always be available
                pass
            callstack.append('')
            callstack.append('')
        logger.error("Nicey Error: %s\n\nTraceback:\n%s" % (args[0], '\n'.join(callstack)))

class NiceyResponseError(NiceyError):
    """
    Raised whenever the Nicey system returns an error response.
    """
    pass

class NiceyRequestError(NiceyError):
    """
    A Nicey request was attempted but could not be completed because there
    was insufficient/incorect data supplied. Basically any error that originates
    from the Nicey Library as opposed to coming from the Nicey system itself.
    """
    pass

class NiceyServerError(NiceyError):
    """
    This exception means that the nicey server is down and/or no connection
    can be made.
    """
    pass

class NiceyInternalError(NiceyError):
    """
    The Nicey library has messed up somehow.
    """
    pass

class RequestError(Exception):
    """
    Raised any time a remote call returns a response with status code other
    than 200.
    """
    pass

class GatewayError(Exception):
    """ Errors returned from API Gateway """
    pass
