class Nicey2Response(object):
    """
    This class has helpers methods that consume nicey response objects
    and sets properties on this class. It is used to return data back in
    to the main bidsite app in a easy and maintainable fashion.

    Nicey2Response:
        purchase_id
        order_id
        transaction_id
        payment_account_id
    """
    def __init__(self):
        self._purchase_id = None
        self._order_id = None
        self._transaction_id = None
        self._payment_account_id = None
        self._customer_id = None
        self._statement_descriptor = None

    @property
    def customer_id(self):
        return self._customer_id
    @customer_id.setter
    def customer_id(self, v):
        self._customer_id = v

    @property
    def transaction_id(self):
        return self._transaction_id
    @transaction_id.setter
    def transaction_id(self, v):
        self._transaction_id = v

    @property
    def payment_account_id(self):
        return self._payment_account_id
    @payment_account_id.setter
    def payment_account_id(self, v):
        self._payment_account_id = v

    @property
    def order_id(self):
        return self._order_id
    @order_id.setter
    def order_id(self, v):
        self._order_id = v

    @property
    def purchase_id(self):
        return self._purchase_id
    @purchase_id.setter
    def purchase_id(self, v):
        self._purchase_id = v

    @property
    def statement_descriptor(self):
        return self._statement_descriptor
    @statement_descriptor.setter
    def statement_descriptor(self, v):
        self._statement_descriptor = v
        
    
    def __unicode__(self):
        return {"customer_id": self.customer_id,
                "transaction_id": self.transaction_id,
                "order_id": self.order_id,
                "payment_account_id": self.payment_account_id,
                "customer_id": self.customer_id,
                "statement_descriptor": self.statement_descriptor}
