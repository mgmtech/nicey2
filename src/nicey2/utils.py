import re

def find_card_details(safe_identifier):
    """
    Takes the nodeValue of the firstChild of SafeIdentifier element
    and returns the last 4 of the CC and the EXP as a dictionary
    """
    last4, exp = re.search(r'(\d+) \((\d\d/\d\d)\)', safe_identifier).groups()    
    return locals()

def dict2tuple(d):
    """ dictionary to tuple helper
    """
    if isinstance(d, dict):
        return tuple(d.items())
    raise Exception("dict2tuple requires a dictionary object, got:%s instread" % type(d))


class NiceyResponse(object):    
    def get_error(self):
        """
        Given a response from nicey, try to determine if there is an error, and then
        extract that error and return it.
        """
        if hasattr(self, "ServiceResponse"):
            if hasattr(self.ServiceResponse, "Error"):
                return self.ServiceResponse.Error
        
            if hasattr(self.ServiceResponse, "Lookup") and not self.ServiceResponse.Lookup:
                return "Empty Response (Order ID not found?)"
    
        if hasattr(self, "Upsell"):
            if hasattr(self.Upsell, "Exception"):
                return self.Upsell.Exception 

        if hasattr(self, "Order"):
            if hasattr(self.Order, "Exception"):
                return self.Order.Exception

            if not (hasattr(self.Order.meta, 'Purchase') or hasattr(self.Order.meta, 'PurchaseReport')):
                return "Nothing for that ID"

        # no error found.
        return None

class NiceyMockResponse(NiceyResponse):
    def get_error(self):
        return None

def dict2obj(d, limit=10):
    """
    Recursive dictionary to object helper
    limit is the level limit. XXX: Not implemented .. yet.
    """

    if isinstance(d, list):
        d = [dict2obj(x) for x in d]

    if not isinstance(d, dict):
        return d

    o = NiceyResponse()

    for k in d:
        o.__dict__[k] = dict2obj(d[k])
    return o
