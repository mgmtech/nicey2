import xml.dom.minidom
import httplib
import base64

from nicey_exceptions import NiceyInternalError, NiceyServerError
from utils import dict2obj

class XMLGatewayInterface(object):
    
    USERAGENT = 'grabswag.com'
    
    def __init__(self, host, ssl=False, auth=False, debug=False):
        """ initalize API call session

        host: hostname (apigateway.tld)
        ssl: True/False
        auth: accept a tuple with (username,password)
        debug: True/False
        """
        self.doc = xml.dom.minidom.Document()
        self.api_host = host
        self.api_ssl = ssl
        self.api_auth = auth
        self.debug = debug

    def reset(self):
        """ Resets the current API session state by resetting the stored XML data """
        self.doc = xml.dom.minidom.Document()

    def set(self, path, child=False, attribute=False):
        """ Accepts a forward slash seperated path of XML elements to traverse and create if non existent.
        Optional child and target node attributes can be set. If the `child` attribute is a tuple
        it will create X child nodes by reading each tuple as (name, text, 'attribute:value') where value
        and attributes are optional for each tuple.

        - path: forward slash seperated API element path as string (example: "Order/Authentication/Username")
        - child: tuple of child node data or string to create a text node
        - attribute: sets the target XML attributes (string format: "Key:Value")
        """
        xml_path = path.split('/')
        xml_doc = self.doc

        # traverse full XML element path string `path`
        for element_name in xml_path:
            # get existing XML element by `element_name`
            element = self.doc.getElementsByTagName(element_name)
            if element: element = element[0]

            # create element if non existing or target element
            if not element or element_name == xml_path[-1:][0]:
                element = self.doc.createElement(element_name)
                xml_doc.appendChild(element)

            xml_doc = element

        if child:
            # create child elements from an tuple with optional text node or attributes
            # format: ((name1, text, 'attribute:value'), (name2, text2))
            if isinstance(child, tuple):
                for obj in child:
                    child = self.doc.createElement(obj[0])
                    if len(obj) >= 2:
                        element = self.doc.createTextNode(str(obj[1]))
                        child.appendChild(element)
                    if len(obj) == 3:
                        a = obj[2].split(':')
                        child.setAttribute(a[0], a[1])
                    xml_doc.appendChild(child)
            # create a single text child node
            else:
                element = self.doc.createTextNode(str(child))
                xml_doc.appendChild(element)

        # target element attributes
        if attribute:
            #checking to see if we have a list of attributes
            if '|' in attribute:
                attributes = attribute.split('|')
            else:
                #if not just put this into a list so we have the same data type no matter what
                attributes = [attribute]

            # adding attributes for each item
            for attribute in attributes:
                attribute = attribute.split(':')
                xml_doc.setAttribute(attribute[0], attribute[1])

    def query(self, path, child=False, attribute=False):
        """ Helper for single command API calls - returns the result right away and handles session reset """
        self.reset()
        self.set(path, child, attribute)
        resp = self.post()
        self.reset()
        return resp

    def parse_xml(self, element):
        """ Parse a XML API Response xml.dom.minidom.Document. Returns the result as dict or string
        depending on amount of child elements. Returns None in case of empty elements
        """
        if not isinstance(element, xml.dom.minidom.Node):
            try:
                element = xml.dom.minidom.parseString(element)
            except: raise

        # return DOM element with single text element as string
        if len(element.childNodes) == 1:
            child = element.childNodes[0]
            if child.nodeName == '#text':
                return child.nodeValue.strip()

        # parse the child elements and return as dict
        root = {}

        for e in element.childNodes:
            t = {}

            if e.nodeName == '#text':
                if not e.nodeValue.strip(): continue

            if e.attributes:
                t['attribute'] = {}
                for attribute in e.attributes.values():
                    t['attribute'][attribute.nodeName] = attribute.childNodes[0].nodeValue

            if e.childNodes:
                if t.has_key('attribute'):
                    t['meta'] = self.parse_xml(e)
                else:
                    if len(e.childNodes) == 1:
                        if e.firstChild.nodeType == xml.dom.Node.CDATA_SECTION_NODE:
                            t = e.firstChild.wholeText
                        else:
                            t = self.parse_xml(e)
                    else:
                        t = self.parse_xml(e)

            if not t:
                t = e.nodeValue

            if root.has_key(e.nodeName):
                if not isinstance(root[e.nodeName], list):
                    tmp = []
                    tmp.append(root[e.nodeName])
                tmp.append(t)
                t = tmp

            root[e.nodeName] = t

        return root

    def post(self, api_uri):
        """ Submits the API request as XML formated string via HTTP POST and parse gateway response.
        This needs to be run after adding some data via `set` or automatically via `query`
        """
        request_body = self.doc.toxml('utf-8')

        if self.debug:
            print 'Connecting to %s/%s' % (self.api_host, api_uri)

        if self.api_ssl:
            api = httplib.HTTPSConnection(self.api_host)
        else:
            api = httplib.HTTPConnection(self.api_host)

        headers = {
            'Host' : self.api_host,
            'Content-type' : 'text/xml; charset="utf-8"',
            'Content-length' : str(len(request_body)),
            'User-Agent' : self.USERAGENT
        }

        # on auth required generated digist and add to header
        if self.api_auth:
            headers['Authorization'] = 'Basic %s' % base64.encodestring(
                self.api_auth[0] + ':' + self.api_auth[1]
            ).strip()

        try:
            api.connect()
        except Exception as exc:
            raise NiceyServerError('Nicey Server unreachable')
        
        api.putrequest('POST', api_uri, skip_host=True)
        api.putheader('Host', self.api_host)
        api.putheader('Content-type', 'text/xml; charset="utf-8"')
        api.putheader("Content-length", str(len(request_body)))
        api.putheader('User-Agent', self.USERAGENT)
        api.endheaders()
        api.send(request_body)

        resp = api.getresponse()
        resp_data = resp.read()

        # parse API call response
        if not resp.status == 200:
            raise NiceyServerError('Gatway returned %i' % resp.status)

        # optional debug output
        if self.debug:
            print '****************'
            print '*** REQUEST: ***'
            print '****************\n%s' % self.doc.toprettyxml()
            
            print '*****************'
            print '*** RESPONSE: ***'
            print '*****************\n%s' % resp_data

        resp_dict = self.parse_xml(resp_data)

        self.query = self.doc.toprettyxml()
        self.response_xml = resp_data

        return resp_dict
    
    def submit(self, uri):
        """
        Post the request through the XMLGateway Interface..
        Returns a dictionary which is then parsed.
        """

        try:
            xml_path_root = self.doc.childNodes
        except AttributeError:
            raise NiceyInternalError("Invalid xml structure..")

        if not xml_path_root:
            raise NiceyInternalError("No xml_path_root")

        try:
            xml_path_root = xml_path_root[0].tagName
        except KeyError:
            raise NiceyInternalError("xml_path_root is invalid")

        # call the interface, catch any errors
        resp = self.post(uri)

        # jombinate the response. ;)
        self.response_dict = resp
        response = dict2obj(resp)
        self.response = response
        
        _dom = xml.dom.minidom.parseString(self.response_xml)
        _dom.normalize()	
        self.response.dom = _dom
        
        return response
