from minimock import Mock
from minimock import mock
from minimock import restore
from minimock import TraceTracker
import unittest
from nicey2.main import Nicey2

try:
    from local_settings import NICEY_HOST, NICEY_PASSWORD, NICEY_USER
    NICEY_MERCHANT = 'MyDoMedia'
except ImportError:
    NICEY_PASSWORD = 'w00tsauce'
    NICEY_HOST = 'dev.durkus.com'
    NICEY_USER = 'mydomedia'
    NICEY_MERCHANT = 'Always Approve'

NICEY_ORDER_ID = None
#raise IndexError(NICEY_HOST, NICEY_PASSWORD, NICEY_USER)

class LiveNicey2(Nicey2):
    API_HOST = NICEY_HOST
    API_USER = NICEY_USER
    API_PASS = NICEY_PASSWORD
    MERCHANT = NICEY_MERCHANT
    WEBSITE = 'billing.mydomedia.com'
    CURRENCY = 'USD'

class TestCase(unittest.TestCase):
    def __init__(self, *args, **kw):
        self.tt = TraceTracker()
        super(TestCase, self).__init__(*args, **kw)

    def setup(self):
        pass

    def setUp(self):
        self.setup()

    def teardown(self):
        pass

    def tearDown(self):
        self.teardown()

    def assertTrace(self, want):
        assert self.tt.check(want), self.tt.diff(want)

__all__ = [
    'Mock',
    'mock',
    'restore',
    'TestCase',
    'Nicey2',
    'LiveNicey2',
    'NICEY_ORDER_ID',
]
