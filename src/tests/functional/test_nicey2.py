"""
Usage: 'python test.py'
or with more debug output: 'python -O test.py'

This file tests both the Nicey library and the live Nicey server.
"""
import datetime

from tests import *

from ordereddict import OrderedDict
from decimal import Decimal

from nicey2.nicey_exceptions import *

class Nicey2Test(TestCase):
    def setup(self):
        self.order_id = None

        self.exp = (datetime.datetime.now() + datetime.timedelta(days=366)).strftime('%m/%y')

        self.customer = OrderedDict()
        self.customer['FirstName']='Larby'
        self.customer['LastName']='Williams'
        self.customer['Email']='demo@example.com'
        self.customer['Phone']='5555551212'
        self.customer['Address']='235 Lincoln Rd'
        self.customer['City']='Miami Beach'
        self.customer['State']='FL'
        self.customer['Zip']='33139'
        self.customer['Country']='US'

        self.address = OrderedDict()
        self.address['FirstName']='Larby'
        self.address['LastName']='Williams'
        self.address['Address']='235 Lincoln Rd'
        self.address['City']='Miami Beach'
        self.address['State']='FL'
        self.address['Zip']='33139'
        self.address['Country']='US'

        self.card = OrderedDict()
        self.card['ccnum']='4111111111111111'
        self.card['ccexp']=self.exp
        self.card['cvv']='123'

        self.card_other = OrderedDict()
        self.card_other['ccnum']='4012888888881881'
        self.card_other['ccexp']=self.exp
        self.card_other['cvv']='123'

        self.item = OrderedDict(
            price='9.99',
            description='tests.functional.test_nicey2',
            ip='128.5.4.3',
        )

    def create_new_order(self, nicey):
        nicey.add_address(self.customer, 'Contact')
        nicey.add_address(self.customer, 'Shipping')
        nicey.add_address(self.customer, 'Billing')
        nicey.add_card(self.card, 'Default', self.address)
        response = nicey.new_purchase(**self.item)
        return response

    def test_bad_api_assert(self):
        self.assertRaises(NiceyError, Nicey2)

    def test_nicey_unreachable_assert(self):
        class BadHostNicey(Nicey2):
            API_HOST = 'unknown.zzz'
            API_USER = 'mydomedia'
            API_PASS = 'w00tsauce'
            MERCHANT = 'Always Approve'
            WEBSITE = 'billing.mydomedia.com'
            CURRENCY = 'USD'

        nicey = BadHostNicey()
        self.assertRaises(NiceyServerError, self.create_new_order, nicey)

    def assert_purchase(self, response):
        self.assertTrue(response)
        self.assertTrue(response.order_id)
        self.assertTrue(response.purchase_id)
        self.assertTrue(response.transaction_id)
        self.assertTrue(response.customer_id)
        self.assertTrue(response.statement_descriptor)
        self.assertTrue(response.approved)

    def test_nicey_response_error(self):
        nicey = LiveNicey2()
        self.card['cvv'] = ''
        self.assertRaises(NiceyResponseError, self.create_new_order, nicey)
        self.card['cvv'] = '123'

    def test_declined(self):
        nicey = LiveNicey2()
        self.item['price'] = '6.66'
        self.assertRaises(NiceyResponseError, self.create_new_order, nicey)
        self.item['price'] = '9.99'

    def test_new_order(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        self.assert_purchase(response)

        # test creating another purchase after we have a nicey id
        nicey.customer_id = response.customer_id
        response = nicey.new_purchase(**self.item)
        self.assert_purchase(response)

    def test_lookup_order(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        self.assertTrue(response)
        self.assertTrue(response.order_id)
        order_response = nicey.lookup_order(response.order_id)
        self.assertTrue(order_response)
        self.assertTrue(order_response.dom)

        resp_dom = order_response.dom
        orders = resp_dom.getElementsByTagName('Order')
        self.assertTrue(orders)

        order = orders[0]
        self.assertEquals(int(order.getAttribute('ID')), response.order_id)
        self.assertEquals(int(order.getAttribute('CustomerID')), response.customer_id)

        purchases = order.getElementsByTagName('Purchase')
        self.assertTrue(purchases)
        purchase = purchases[0]

        self.assertEquals(int(purchase.getAttribute('ID')), response.purchase_id)
        self.assertEquals(int(purchase.getAttribute('OrderID')), response.order_id)

    def test_add_cc(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        response = nicey.add_card(self.card_other, 'Default', contact_id)

        self.assertTrue(int(response))

    def test_lookup_customer(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        nicey.customer_id = response.customer_id

        response = nicey.lookup_customer()

        self.assertEquals(int(response.Customer.attribute.ID), nicey.customer_id)

    def test_lookup_by_last4(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        response = nicey.add_card(self.card_other, 'Default', contact_id)

        response = nicey.lookup_customers_by_last4('1881')
        self.assertEquals(response, [customer_id])

    def test_lookup_payment_account(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        response = nicey.add_card(self.card_other, 'Default', contact_id)

        response = nicey.lookup_payment_account(response)
        self.assertEquals(response['last4'], '1881')
        self.assertEquals(response['exp'], self.exp)

    def test_get_latest_order(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        nicey.customer_id = response.customer_id

        response = nicey.get_latest_order()

        self.assertEquals(response['card_num'], '1111')
        self.assertEquals(response['zipcode'], '33139')
        self.assertEquals(response['amount'], '9.99')

    def test_lookup_transaction(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        transaction = nicey.lookup_transaction(response.transaction_id)

        self.assertEquals(transaction.get('id'), int(response.transaction_id))
        self.assertEquals(transaction.get('approved'), True)
        self.assertEquals(transaction.get('amount'), Decimal('9.99'))

    def test_refund_amount_high_raise(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        nicey.customer_id = response.customer_id

        args = dict(
            transaction_id = response.transaction_id,
            amount = Decimal('10.00')
        )
        self.assertRaises(ValueError, nicey.refund_transaction, **args)

    def test_refund_transaction(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)
        nicey.customer_id = response.customer_id

        refund_transaction = nicey.refund_transaction(response.transaction_id)
        self.assertEquals(refund_transaction.get('id'), int(response.transaction_id))
        self.assertEquals(refund_transaction.get('approved'), True)

    def test_add_contact(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        self.assertTrue(int(contact_id))

    def test_modify_cc(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        response = nicey.add_card(self.card_other, 'Default', contact_id)

        response = nicey.modify_card(response, active=False)

        response = nicey.list_cards()
        card = response[0]
        self.assertEquals(card['active'], False)

    def test_modify_cc_default(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        contact_id = nicey.add_contact(self.address)
        response = nicey.add_card(self.card_other, 'Secondary', contact_id)

        response = nicey.modify_card(response, active=True, default=True)

        response = nicey.list_cards()
        card = response[0]
        card_alt = response[1]

        self.assertEquals(card['nickname'], 'Secondary')
        self.assertEquals(card['default'], True)
        self.assertEquals(card_alt['default'], False)

    def test_list_cc(self):
        nicey = LiveNicey2()
        response = self.create_new_order(nicey)

        nicey.customer_id = response.customer_id
        response = nicey.list_cards()

        self.assertEquals(len(response), 1)
        card = response[0]

        self.assertEquals(card['nickname'], 'Startup Account')
        self.assertEquals(card['last4'], '1111')
        self.assertEquals(card['exp'], self.exp)
