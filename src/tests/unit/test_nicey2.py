"""
Unit tests for the Nicey library.

These MUST NOT make any calls to the Nicey server.
"""

from tests import *

from nicey2.nicey_exceptions import *
from nicey2.main import *

class PurchasingTest(TestCase):
    def setup(self):
        super(PurchasingTest, self).setup()

    def teardown(self):
        super(PurchasingTest, self).teardown()

    def test_check_approved(self):
        transaction_node = Mock('Transaction', tracker=self.tt)
        response = Mock('response', tracker=self.tt)
        response.dom.getElementsByTagName.mock_returns_func = lambda n: \
            [transaction_node] if n == 'Transaction' else []
        transaction_node.getAttribute = lambda n: \
            'false' if n == 'Approved' else None

        nicey = LiveNicey2()
        self.assertRaises(NiceyResponseError, nicey.check_response_errors, response)

    def test_check_error(self):
        error_node = Mock('Error', tracker=self.tt)
        response = Mock('response', tracker=self.tt)
        response.dom.getElementsByTagName.mock_returns_func = lambda n: \
            [error_node] if n == 'Error' else []
        error_node.firstChild.nodeValue = 'Test Error'

        nicey = LiveNicey2()
        self.assertRaises(NiceyResponseError, nicey.check_response_errors, response)

    def test_check_exception(self):
        error_node = Mock('Exception', tracker=self.tt)
        response = Mock('response', tracker=self.tt)
        response.dom.getElementsByTagName.mock_returns_func = lambda n: \
            [error_node] if n == 'Exception' else []
        error_node.firstChild.nodeValue = 'Test Error'

        nicey = LiveNicey2()
        self.assertRaises(NiceyResponseError, nicey.check_response_errors, response)

    def test_exception_message(self):
        exc = NiceyError('Test Error')
        self.assertEquals(str(exc), 'Test Error')
